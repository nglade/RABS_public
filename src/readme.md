### Requirements
clingo 4 or 5

### Usage
```
clingo hopfield.lp node-order.lp inference.lp [<additional_modules.lp>] -n0 --project --opt-mode=optN [-c d=<net_size>]
```
where:
* `<additional_modules.lp>` are added if necessary:
  * `hopfield-min.lp`: discard non-canonical models (ones with equivalent better models)
  * `fonctions_bool_spec.lp`: only consider weights/thresholds corresponding to specific boolean functions
  * `min-weight.lp`: ensure canonicality by minimizing sum of absolute values of weights

  at least 1 must be provided (otherwise, the answer set would be huge)
* `-c d=<size>`: change `d` constant to set network size (number of nodes) (`d=3` by default)

To change the sequence (motif), it is necessary to update `motif(Name, Step, Value)` atoms in `inference.lp` module.
`Step` should always start from 1. Any `Name` is acceptable.

### Modules
* `hopfield.lp`: hopfield-like structure & dynamics
* `node-order.lp`: impose node order to limit certain repetitive models (not exhaustively though)
* `inference.lp`: problem instance, to define network size, *required motif*
* optional modules as mentioned above
