% order(I): I can be an order, from 1 to size of networks
order(1..D) :- nb_nodes(D).

% n_order(N, I): node N has a unique order I
% when N is the root, I = 1
n_order(N, I) :- root(N), I = 1.
1 <= {n_order(N, I) : order(I)} <= 1 :- node(N).

:- n_order(N1, I1), n_order(N2, I2), N1 != N2, I1 == I2. % 2 different nodes must have different orders

% tree_parent(N, N0): N0 is the parent of N in the spanning tree
% each node N except the root has 1 unique parent N0
1 <= {tree_parent(N, N0) : non_nul_self_edge(N0, N)} <= 1 :- node(N), not root(N).

% level(N, L): L is the level of N in the spanning tree, determined by the level of its parent + 1
% L is limited by number of nodes
level(N, L) :- node(N), root(N), L = 0.
level(N, L) :- tree_parent(N, N0), level(N0, L0), L = L0 + 1, nb_nodes(D), L < D.

% possible_level(L): L is a possible level, from 0 to nb_nodes - 1
possible_level(0..L) :- nb_nodes(D), L = D - 1.
% each node must have 1 unique level
1 <= {level(N, L) : possible_level(L)} <= 1 :- node(N).

:- tree_parent(N, N1), non_nul_self_edge(N2, N), level(N1, L1), level(N2, L2),
    not L1 <= L2. % if N1 is the parent of N, N1 must have smallest level amongst all nodes targeting N
:- tree_parent(N1, N2), tree_parent(N2, N1). %- N1 cannot be parent of N2 if N2 is parent of N1

% inf_order_l(N1, N2): level of N1 is smaller than level of N2
inf_order_l(N1, N2) :- level(N1, L1), level(N2, L2), L1 < L2.

:- inf_order_l(N1, N2), n_order(N1, I1), n_order(N2, I2), not I1 < I2. % N1 and N2 are ordered correctly according to level

% inf_order_p(N1, N2): N1 and N2 is on the same level but have different parents N01 and N02, and N01's order is inferior than N02's order
inf_order_p(N1, N2) :- level(N1, L), level(N2, L), tree_parent(N1, N01), tree_parent(N2, N02), N02 != N01,
    n_order(N01, I01), n_order(N02, I02), I01 < I02.

:- inf_order_p(N1, N2), n_order(N1, I1), n_order(N2, I2), not I1 < I2.

% inf_order_w(N1, N2): N1 and N2 have the same parent N0 in the spanning tree
% and W1 from N0 to N1 is smaller than W2 from N0 to N2
inf_order_w(N1, N2) :- tree_parent(N1, N0), tree_parent(N2, N0),
    weight(N0, N1, W1), weight(N0, N2, W2),
    W1 < W2.

:- inf_order_w(N1, N2), n_order(N1, I1), n_order(N2, I2), I1 >= I2. % N1 and N2 are ordered correctly according to weight

% inf_order_t(N1, N2): N1 and N2 have the same parent N0 in the spanning tree,
% the same weight from their parent, and threshold T1 of N1 is smaller than threshold T2 of N2
inf_order_t(N1, N2) :- tree_parent(N1, N0), tree_parent(N2, N0),
    weight(N0, N1, W), weight(N0, N2, W),
    threshold(N1, T1), threshold(N2, T2),
    T1 < T2.

:- inf_order_t(N1, N2), n_order(N1, I1), n_order(N2, I2), I1 >= I2. % N1 and N2 are ordered correctly according to threshold

% smallest_target(N, N1): N1 is the smallest node affected by N
% smallest_target(N, I') :- node(N), I' = #min { I0 : non_nul_self_edge(N, N0), n_order(N0, I0) }.
% inf_order_tar(N1, N2) :- tree_parent(N1, N0), tree_parent(N2, N0),
%     weight(N0, N1, W), weight(N0, N2, W),
%     threshold(N1, T), threshold(N2, T),
%     smallest_target(N1, I1'), smallest_target(N2, I2'),
%     I1' < I2'.
% :- inf_order_tar(N1, N2), n_order(N1, I1), n_order(N2, I2), not I1 < I2.

% ambiguial_order(N1, N2): when N1 and N2 cannot be compared using above criteria
ambiguous_order(N1, N2) :- N1 != N2,
    tree_parent(N1, N0), tree_parent(N2, N0),
    weight(N0, N1, W), weight(N0, N2, W),
    threshold(N1, T), threshold(N2, T).
    % threshold(N1, T), threshold(N2, T),
    % smallest_target(N1, I'), smallest_target(N2, I').

:- C1 = #count {1 : non_nul_self_edge(N1, N)}, node(N1),
    C2 = #count {1 : non_nul_self_edge(N, N2)}, node(N2),
    not C1 + C2 > 0. % for any node N, it must target at least 1 other node N2,
                      % or must be targeted by at least 1 other node N1
