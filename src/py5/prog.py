# clingo4
from gringo import Control, Model, Fun
from time import time
import sys

class Solver:
    def __init__(self, nb_nodes, motif, files):
        self.solutions = []
        self.opt = []
        self.ambi_indices = []
        self.nb_nodes = nb_nodes
        self.motif = motif
        self.ctl = Control([
            '-c', 'nb_nodes={0}'.format(self.nb_nodes),
            '-c', 'motif_size={0}'.format(len(self.motif))
        ])
        self.ctl.conf.solve.models = 0
        self.ctl.conf.solve.opt_mode = 'optN'
        self.ctl.conf.solve.project = 1

        for f in files:
            self.ctl.load(f)
        self.ctl.ground([('base', [])])
        for i, c in enumerate(motif):
            x = Fun('motif_init', [i + 1, int(c)])
            self.ctl.assign_external(x, True)
        self.ctl.assign_external(Fun('max_size_motif', [len(self.motif)]), True)

    def solve(self):
        self.solutions = []
        self.ambi_indices = []
        self.opt = []
        self.ctl.cleanup_domains()
        self.ctl.solve(on_model = self.on_model)
        return self.solutions, self.ambi_indices

    def on_model(self, model):
        # clingo5 has optimality_proven, simpler to check
        opt = model.optimization()
        not_optimized = False
        if len(self.opt) < len(opt):
            not_optimized = True
        else:
            # FIXME for optimization problem, the first result (with the optimal cost) is repetitve?
            # repetition will be removed during unification phase anyway
            for i in range(0, len(opt)):
                if opt[i] < self.opt[i]:
                    not_optimized = True
                    break

        # if new solution has better cost --> stored solutions are not optimized -->  discarded
        if not_optimized:
            self.opt = opt
            self.solutions = []
            self.ambi_indices = []

        ansset = []
        ambi = False
        for atom in model.atoms(Model.ATOMS):
            if atom.name() in ['weight', 'threshold', 'ambiguous_order', 'similar_motif']:
                ansset.append(atom)
            if atom.name() == 'val' and atom.args()[1] == 1:
                ansset.append(atom) # for initial state
            if atom.name() in ['ambiguous_order', 'similar_motif']:
                ambi = True

        self.solutions.append(ansset)
        if ambi:
            self.ambi_indices.append(len(self.solutions) - 1)

class Unifier:
    def __init__(self, nb_nodes, files):
        self.solutions = []
        self.nb_nodes = nb_nodes
        self.undo_external = []
        self.ctl = Control([
            '-c', 'd={0}'.format(self.nb_nodes)
        ])

        for f in files:
            self.ctl.load(f)
        self.ctl.ground([('base', [])])

    def solve(self, net1, net2):
        for x in self.undo_external:
            self.ctl.assign_external(x, False)
        self.undo_external = []
        self.solutions = []

        self.ctl.cleanup_domains()
        for x in net1:
            if x.name() == 'similar_motif' and len(x.args()) == 3:
                x1 = Fun('similar_motif', x.args()[1:])
            else:
                x1 = x
            self.undo_external.append(x1)
            self.ctl.assign_external(x1, True)
        for x in net2:
            if x.name() == 'weight':
                x1 = Fun('weight_p', x.args())
            if x.name() == 'threshold':
                x1 = Fun('threshold_p', x.args())
            self.undo_external.append(x1)
            self.ctl.assign_external(x1, True)

        self.ctl.solve(on_model = self.on_model)
        return self.solutions

    def on_model(self, model):
        ansset = []
        same = False
        for atom in model.atoms(Model.ATOMS):
            if atom.name() == 'appar':
                same = True
        if same:
            for atom in model.atoms(Model.ATOMS):
                if atom.name() in ['appar', 'weight', 'weight_p', 'threshold', 'threshold_p']:
                    ansset.append(atom)
                if atom.name() == 'val' and atom.args()[1] == 1:
                    ansset.append(atom)

        self.solutions = ansset

nb_nodes = 3
motif = '10011'
files = [
    'hopfield.lp',
    'node-order.lp',
    'fonctions_bool_spec.lp',
    'min-weight.lp',
    'hopfield-min.lp',
    'target.lp'
]

print '>>>> Phase 1'
print '>>> Motif:', motif

st = time()
solver = Solver(nb_nodes, motif, files)
solutions, ambi_indices = solver.solve()
ed = time()

for i, s in enumerate(solutions):
    print '> Solution {0}\n{1}\n'.format(i, s)

print '> Phase 1 took {0}s'.format(ed - st)

print '>>>> Phase 2'
files2 = ['unif.lp']

st = time()
unifier = Unifier(nb_nodes, files2)
repeated = set()
if len(solutions) > 1 and len(ambi_indices) > 1:
    print '>>> Ambi size:', len(ambi_indices)
    for i in range(0, len(ambi_indices) - 1):
        for j in range(i + 1, len(ambi_indices)):
            sys.stdout.write('.')
            s2 = unifier.solve(solutions[ambi_indices[i]], solutions[ambi_indices[j]])
            if len(s2) > 0:
                repeated.add(ambi_indices[j])
ed = time()
sys.stdout.flush()
print

final = [solution for i, solution in enumerate(solutions) if not i in repeated]

for i, s in enumerate(final):
    print '> Solution {0}\n{1}\n'.format(i, s)

print '> Phase 2 took {0}s'.format(ed - st)
