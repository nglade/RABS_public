### Requirements
* python 2.7 (not tested yet for python 3)
* python modules of clingo 4 (not working for clingo 5 due to massive api changes)
  * API reference: http://potassco.sourceforge.net/gringo.html
  * A way to make clingo library accessible in Python is to use `PYTHONPATH` environment variable
    E.g. add the following line to `~/.bashrc` or `~/.zshrc`
    ```
    export PYTHONPATH="/path/to/clingo/build/release/python:$PYTHONPATH"
    ```
    (the `/path/to/clingo` part must be replaced in according to the directory structure of the target machine)

### Usage
```
python prog.py
```
* ASP modules (solving phase) are defined using `files` variable in `prog.py`, added or removed as needed.
  For description of ASP modules, see `readme.md` in the upper directory
* Network size is defined using `nb_nodes` variable in `prog.py`
* Sequence is defined using `motif` variable in `prog.py`
