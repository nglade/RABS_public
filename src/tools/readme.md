# Serialize literal-style parameters
## Usage

Input must be supplied via piping

```
cat input.txt | transform.py [-d <network_size>] [-o <output_file>]
```

Options:
* `-d`: network size, default to `3`
* `-o`: output file name, default to `./out.log`

## Output format
```
<network_size> <init_values> <weights_row_by_row> <thresholds>
```

### Example
```
[weight(3,2,-1), weight(1,3,1), weight(1,2,1), weight(2,3,1), weight(1,1,1), weight(2,1,1), weight(3,3,1), weight(2,2,-1), weight(3,1,-1), threshold(3,1), threshold(1,0), threshold(2,-1), val(1,1,1), val(3,1,0), val(2,1,0)]
```
is transformed into
```
3 1 0 0 1 1 1 1 -1 1 -1 -1 1 0 -1 1
```
