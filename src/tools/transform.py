import argparse
import sys
import re

parser = argparse.ArgumentParser()
parser.add_argument('-o', help='output file')
parser.add_argument('-d', help='network size')
args = parser.parse_args()

if args.d:
    nb_nodes = int(args.d)
else:
    nb_nodes = 3

f = open(args.o or 'out.log', 'w')

# structure: nb_nodes -> init values -> weights (row by row) -> thresholds
buf_size = 1 + nb_nodes + nb_nodes**2 + nb_nodes
buf = [None] * buf_size

p2 = False
for line in sys.stdin:
    if not p2 and re.search('Phase 2', line):
        p2 = True
    if p2 and (re.search('weight\\(\d+,\d+,-?\d+\\)', line) or re.search('threshold\\(\d+,-?\d+\\)', line)):
        buf = [None] * buf_size
        buf[0] = nb_nodes

        for i in range(0, nb_nodes):
            n1 = i + 1
            v = re.search('val\\({0},1,(-?\d+)\\)'.format(n1), line)
            # if not v:
            #     break
            buf[1 + i] = int(v.group(1))

            t = re.search('threshold\\({0},(-?\d+)\\)'.format(n1), line)
            # if not t:
            #     break
            buf[1 + nb_nodes + nb_nodes**2 + i] = int(t.group(1))
            
            for j in range(0, nb_nodes):
                n2 = j + 1
                w = re.search('weight\\({0},{1},(-?\d+)\\)'.format(n1, n2), line)
                # if not w:
                #     break
                buf[1 + nb_nodes + i * nb_nodes + j] = int(w.group(1))
        s = ' '.join(str(e) for e in buf)
        f.write('{0}\n'.format(s))
